package org.example;


import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nhập vào một số nguyên trong khoảng từ 1 đến 100: ");
        int soTuBanPhim = scanner.nextInt();

        DoanSo thread1 = new DoanSo("Thread 1", soTuBanPhim);
        DoanSo thread2 = new DoanSo("Thread 2", soTuBanPhim);
        thread1.start();
        thread2.start();

    }

}