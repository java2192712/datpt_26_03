package org.example;

import java.util.Random;

public class DoanSo extends Thread{
    private int soCanDoan;

    public DoanSo(String name, int soCanDoan) {
        super(name);
        this.soCanDoan = soCanDoan;
    }

    @Override
    public void run() {
        Random random = new Random();
        int doan;
        int soLan = 0;
        do {
            doan = random.nextInt(100) + 1;
            soLan++;
            System.out.println(Thread.currentThread().getName() + " đoán số: " + doan);
        } while (doan != soCanDoan);
        System.out.println(Thread.currentThread().getName() + " đã đoán đúng số " + soCanDoan + " sau " + soLan + " lần đoán.");
    }

}
